provider "aws" {
    region = "us-east-2"
}


provider "kubernetes" {
    # load_config_file = "false"
    host = data.aws_eks_cluster.panel-cluster.endpoint
    token = data.aws_eks_cluster_auth.panel-cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.panel-cluster.certificate_authority.0.data)
}