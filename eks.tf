module "eks-panel" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"
  # insert the 15 required variables here
  cluster_name = "eks-panel-cluster"
  cluster_version = "1.21"
  
  subnets = module.eks-panel-vpc.private_subnets
  vpc_id = module.eks-panel-vpc.vpc_id
  tags = {
      environment = "development"
      application = "panel"
  }

  worker_groups = [
      {
          instance_type = "t2.small"
          name = "worker-group-1"
          asg_desired_capacity = 2
      },
      {
          instance_type = "t2.medium"
          name = "worker-group-2"
          asg_desired_capacity = 1
      }
  ]
}