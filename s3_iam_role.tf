resource "aws_iam_role" "s3_access_role" {
    name                  = "S3TestAccessRole"
    assume_role_policy    = jsonencode(
        {
            Statement = [
                {
                    Action    = "sts:AssumeRoleWithWebIdentity"
                    Condition = {
                        StringEquals = {
                            "oidc.eks.us-east-2.amazonaws.com/id/6E5CE4A5A29C87ABC68F6BC20C1A1168:sub" = "system:serviceaccount:default:app"
                        }
                    }
                    Effect    = "Allow"
                    Principal = {
                        Federated = "arn:aws:iam::092201141815:oidc-provider/oidc.eks.us-east-2.amazonaws.com/id/6E5CE4A5A29C87ABC68F6BC20C1A1168"
                    }
                },
            ]
            Version   = "2012-10-17"
        }
    )
  
}