output "aws_availability_zones_azs" {
    value = data.aws_availability_zones.azs.names
}

output "aws_vpc_subnets" {
    value = module.eks-panel-vpc.private_subnets
}