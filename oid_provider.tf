resource "aws_iam_openid_connect_provider" "eks-oidc" {
    
    client_id_list  = [
        "sts.amazonaws.com",
    ]
    
   
    thumbprint_list = [
        "9e99a48a9960b14926bb7f3b02e22da2b0ab7280",
    ]
    url             = "https://oidc.eks.us-east-2.amazonaws.com/id/6E5CE4A5A29C87ABC68F6BC20C1A1168"
}