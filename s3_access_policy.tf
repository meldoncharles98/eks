resource "aws_iam_policy" "s3_access" {
    name      = "S3TestAccess"
    policy    = jsonencode(
        {
            Statement = [
                {
                    Action   = [
                        "s3:GetObject",
                    ]
                    Effect   = "Allow"
                    Resource = [
                        "arn:aws:s3:::eks-gcs-bucket/*",
                    ]
                },
            ]
            Version   = "2012-10-17"
        }
    )
  
}