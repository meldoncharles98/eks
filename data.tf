data "aws_eks_cluster" "panel-cluster" {
    name = module.eks-panel.cluster_id
}

data "aws_eks_cluster_auth" "panel-cluster" {
    name = module.eks-panel.cluster_id
}

data "aws_availability_zones" "azs" {
    state = "available"
}